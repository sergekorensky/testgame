<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property string $user
 * @property int $task_id
 * @property int $result
 *
 * @property Task $task
 */
class Game extends \yii\db\ActiveRecord
{   
    const INFO = 'Попробуйте из данных слов восстановить оригинальное высказывание.';
    const SUCCESS = 'Вы распознали замысел автора';
    const ERROR = 'Увы, но автор думал иначе';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user'], 'required'],
            [['task_id', 'result'], 'integer'],
            [[ 'result'],  'in', 'range'=>[0,1]],
            [['user'], 'string', 'max' => 255],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'task_id' => 'Task ID',
            'result' => 'Result',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    
    public static function statistic($user)
    {
        $games = static::find()->where(['user'=>$user])->asArray()->all();
        $count = count($games);
        $by_res = ArrayHelper::map($games,'id','task_id','result');
        $fails= count($by_res[0]);
        $wins= count($by_res[1]);
        $by_task = ArrayHelper::map($games,'id','result','task_id');
        array_walk($by_task, function(&$item, $key){$item = count($item);});
        //return $games;
        return ['games'=>$count,
                'wins'=>($wins/$count),
                'fails'=>($fails/$count),
                'by_task'=>$by_task
                ];
    }
}
