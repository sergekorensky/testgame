<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $content
 *
 * @property Game[] $games
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['content'], 'match', 'pattern' => '/^[А-ЯЁ\-\s\w\/,:;\.]+$/uis'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Предложения  автора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['task_id' => 'id']);
    }
    
    public function beforeSave($insert) {
        if (!$insert) {
            $word_pattern = '/[А-ЯЁ\-\w\/,:;\.]+/ui';
            if (preg_match_all($word_pattern, $this->content, $result))
               {
                    $words = $result[0];
                    if (count($words) > 2) $this->content = implode(' ', $words); else return false;              
               }
            else return false;
        }
        return parent::beforeSave($insert);
    }
}
