<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Game */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;

?><?php Pjax::begin(); ?>
<div class="game-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::encode($info) ?></p>

    <?= $this->render('_game', [
        'model' => $model,
         'task'=>$task,
         'words'=>$words,
         'statistic'=>$statistic
    ]) ?>

</div><?php Pjax::end(); ?>
