<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\Game;

/* @var $this yii\web\View */
/* @var $model app\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php //Pjax::begin(); ?>    
    <?php echo Html::beginForm('','post',['data-pjax'=>1]); ?>
    <?php
    if($model->id) {
        //print_r($model->result); 
        if ($model->result)
            $msg = Game::SUCCESS; 
        else
            $msg = Game::ERROR;
        echo '<h3>'.Html::encode($msg).':</h3>';
        echo '<h4 class="word">'.Html::encode($task->content).'</h4>';       
        
        //print_r($statistic);
        echo '<h4 class="stat">Число игр ='.$statistic['games'] .'</h4>';
        echo '<h4 class="stat">Среднее число побед ='.$statistic['wins'] .'</h4>';
        echo '<h4 class="stat">Среднее число поражений ='.$statistic['fails'] .'</h4>';
        echo Html::a('Следующее задание', [''], 
    ['class' => 'btn btn-success', 'data'=>['pjax'=>0], ]);
    
    }else{
            
    ?>
    
        <h3>Доступные слова</h3>
        <div class="form-group" id="words">
           <?php 
            foreach($words as $word) echo Html::tag('span', Html::encode($word), ['class' => 'word']);
           
           ?>           
            
        </div>
        <h3 style="clear:both;">Ваш вариант</h3>
        <div class="form-group" id="answer">
                    
            
        </div>
        <div class="form-group" style="clear:both;">
           <?= Html::textarea('answer', '',['rows'=>10,'cols'=>100,'id'=>'answer2']); ?>
           <?= Html::hiddenInput('task_id',$task->id); ?> 
            
        </div>
    
    
        <div class="form-group">
            <?= Html::submitButton('Ok', ['class' => 'btn btn-success']) ?>
        </div>
        <?php } ?>
    <?php echo Html::endForm(); ?>
    <?php //Pjax::end(); ?>
</div>

<?php
$css='
#words, #answer {min-height:40px;}
.word, .word2 {margin:10px;padding:5px;border-radius:10%;float:left;}
.word {background-color:lightgreen;}
.word2, .stat {background-color:lightgrey;}
.stat {margin:10px;padding:5px;border-radius:10%;clear:both;}
#answer2 {display:none;}
';
$js='
$(document).ready(function(){
$(".word,.word2").bind("click",moveEl);

function moveEl(event){
var el=$(this);
el.detach();
el.toggleClass("word").toggleClass("word2");
if (el.hasClass("word2")) 
    el.appendTo("#answer");
else
    el.appendTo("#words");
  $("#answer2").html(""); 
  $("#answer .word2").each(function(){
    $("#answer2").append($(this).text()+" ");
  });  
}
});
';
$this->registerCss($css);
$this->registerJs($js, \yii\web\View::POS_END);

?>
