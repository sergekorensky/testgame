<?php

use yii\db\Migration;

/**
 * Class m180604_171806_game_table
 */
class m180604_171806_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 ENGINE=InnoDB';
        }
        $this->createTable('game', [
            'id' => $this->primaryKey(),            
            'user' => $this->string()->notNull(),//user_login
            'task_id'=>$this->integer(),
            'result'=>$this->integer()->notNull()->defaultValue(0),
            
        ], $tableOptions);
        
        $this->addForeignKey('fk_game_task','{{%game}}',
        'task_id', '{{%task}}', 'id', 'CASCADE', 'NO ACTION');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m180604_171806_game_table cannot be reverted.\n";
        $this->dropTable('game');

        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_171806_game_table cannot be reverted.\n";

        return false;
    }
    */
}
