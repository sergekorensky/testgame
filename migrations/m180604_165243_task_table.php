<?php

use yii\db\Migration;

/**
 * Class m180604_165243_task_table
 */
class m180604_165243_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 ENGINE=InnoDB';
        }
        $this->createTable('task', [
            'id' => $this->primaryKey(),            
            'content' => $this->text()->notNull(),
        ], $tableOptions);
        
        $text = "Как вы, возможно, уже знаете, Yii 2.0 был переписан с нуля. Это решение было принято, так как мы хотели получить отличный PHP фреймворк, который сохранит простоту и расширяемость Yii и, в то же время, будет использовать свежие технологии и возможности, чтобы стать ещё лучше. Сегодня мы рады сообщить, что цель достигнута. ";
        
        $tasks = explode('. ', $text);
        
        foreach ($tasks as $task)
            
            if ($task != '')
            $this->insert('task', [            
            'content' => $task,
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       // echo "m180604_165243_task_table cannot be reverted.\n";
        $this->dropTable('task');
       // return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_165243_task_table cannot be reverted.\n";

        return false;
    }
    */
}
