<?php

namespace app\controllers;

use Yii;
use app\models\Game;
use app\models\Task;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    /**
     * {@inheritdoc}
     */
     
    public $defaultAction = 'create';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [                    
                    [
                        'actions' => ['create'],
                        'allow' =>true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' =>true,
                        'roles' => ['@'],
                        'matchCallback' => function () {   
                       
                            return (Yii::$app->user->identity->username == 'admin');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Game::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Game model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Game();
        $statistic=[];        
        $task = Task::find()->orderBy(new Expression('rand()'))->one();
        $words = explode(' ', $task->content);
        shuffle($words);
        
        $info = Game::INFO;
        $title = 'Новая игра';
                       
        if ($answer=Yii::$app->request->post('answer')) {
            $answer = trim($answer);
            $answer = Html::decode($answer);
            $task_id= Yii::$app->request->post('task_id');
            $task = Task::findOne($task_id);
            if($task->content == $answer)
                $model->result =1;
            else
                $model->result =0;
            $model->user = Yii::$app->user->identity->username;
            $model->task_id = $task_id;
            
            if ($model->validate()&& $model->save()) {
                //return $this->render('result', ['model' => $model, ]);
                $statistic = Game::statistic($model->user);
                $title = 'Результаты игры';
                $info = '';        
                
            }
        
        }

        return $this->render('new', [
            'model' => $model,
            'task'=>$task,
            'words'=>$words,
            'statistic'=>$statistic,
            'info'=> $info,
            'title'=>$title,
        ]);
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
